#Classe qui s'occupe de l'access a la base de données de la librarie
#Propose des functions haut-niveau, et execute le code SQL correspondant

import sqlite3

class Librarie:

   #fichierDb: le fichier a utiliser pour stocker les infos
   def __init__(self, fichierDb):
      #self.conn est la connection a la base de données
      self.conn = sqlite3.connect(fichierDb)
      
      #Si c'est un nouveau fichier, on va creer les tables pour stocker les données
      self.creerTablesSiBesoin();

   
   def creerTablesSiBesoin(self):
      #on crée un object pour pouvoir interair avec la base de données. Ca s'appelle un cursor
      c = self.conn.cursor()

      #on crée une table de nom 'livre' avec un seul champ: 'titre'
      c.execute('CREATE TABLE IF NOT EXISTS livre(titre)')

      #on enregistre les changements
      self.conn.commit()

   def listerTousLesLivres(self):
      #on crée un object pour pouvoir interair avec la base de données. 
      c = self.conn.cursor()
      # on execute une requete demandant toutes le lignes da la table 'livre'
      c.execute('SELECT * FROM livre')
      #on retourne tous les lignes obtenues
      return c.fetchall()

   def ajouterLivre(self, titre):
      c = self.conn.cursor()
      #on ajoute une nouvelle ligne dans la table livre avec le titre donné
      modificationsAvantAjout = self.conn.total_changes;
      c.execute("INSERT INTO livre(titre) VALUES (?)", (titre,))
      #on enregistre les changements
      self.conn.commit()
      return self.conn.total_changes - modificationsAvantAjout;

   def chercherLivreAvecTitre(self, titre):
      c = self.conn.cursor()
      c.execute('SELECT * FROM livre WHERE titre LIKE ?', (titre, ))
      return c.fetchall()

   def supprimerLivre(self, titre):
      c = self.conn.cursor()
      modificationsAvantSupp = self.conn.total_changes;
      c.execute("DELETE FROM livre WHERE titre=?", (titre,))
      self.conn.commit()
      return self.conn.total_changes - modificationsAvantSupp;
