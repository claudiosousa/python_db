#le nom de fichier a utiliser pour la base de données
DATABASE_FILE = 'librarie.db';
    
import os
import librarieDb

#librarieDb est un acces à la base de données.
#Regarder le code de librarieDb.py"
librarie = librarieDb.Librarie(DATABASE_FILE);

#cette function est appellée quand le utilisateur choisi l'action 'liste'
def listerTousLesLivres():
    os.system('cls')
    print('Liste de tous les livres:')
    livresTrouves = librarie.listerTousLesLivres()
    for livre in livresTrouves:
        print('  - '+livre[0])
    print()
    input("presser enter pour continuer...")
    
#cette function est appellée quand le utilisateur choisi l'action 'chercher'
def chercherLivreAvecTitre():
    os.system('cls')
    titreAChercher = input("Entrer partie du titre du livre a chercher: ")
    print('Résultat de la recherche:')
    livresTrouves = librarie.chercherLivreAvecTitre('%'+titreAChercher+'%')
    for livre in livresTrouves:
        print('  - '+livre[0])
    print()
    input("presser enter pour continuer...")

#cette function est appellée quand le utilisateur choisi l'action 'ajout'"
def ajouterLivre():
    os.system('cls')
    nouveauTitre = input("Entrer titre du livre a ajouter: ")
    ajoutés = librarie.ajouterLivre(nouveauTitre);
    if (ajoutés == 1):
        print("Livre ajouté!")
    else:
        print("Aucun livre ajouté!")
    print()    
    input("presser enter pour continuer...")

#cette function est appellée quand le utilisateur choisi l'action 'supp'"
def supprimerLivre():
    os.system('cls')
    titreASupprimer = input("Entrer le titre exact du livre a supprimer: ")
    effaces = librarie.supprimerLivre(titreASupprimer)
    if (effaces>0):
        print('Livres effacés: '+str(effaces))
    else:
        print('Aucun livre effacé')
    print()
    input("presser enter pour continuer...")
    


action = ''
while action != 'stop':
    #Nettoyer l'ecran
    os.system('cls')

    #Lire action a executer
    action = input("""Ecrire action a executer:
    liste    -> lister les livres existants
    chercher -> pour chercher un livre avec un bout du titre
    ajout    -> pour ajouter un livre
    supp     -> pour supprimer un livre
    stop     -> sortir de l'appplication
: """)
    if action == "liste":
        listerTousLesLivres()       
    elif action == "chercher":
        chercherLivreAvecTitre()
    elif action == "ajout":
        ajouterLivre()
    elif action == "supp":
        supprimerLivre()
    
    
    
    
