# README #

Exemple d'application démontrant l'acces à une base de données sqlite en python.

L'exemple gere une liste de livres.

### Prérequis ###

* [Python 3.4.3](https://www.python.org/ftp/python/3.4.3/python-3.4.3.msi)

### Instalation ###

* (installer python)
* Telecharger l'application [lien](https://bitbucket.org/claudiosousa/python_db/get/ae842dd91e0e.zip)
* Extraire les fichiers telechargés  ![unzip.png](https://bitbucket.org/repo/XKyxgx/images/1492844414-unzip.png)

### Execution de l'application ###

* Aller a l'endroit ou les fichier ont été extraits et double-cliquer sur librarie.py ![click.png](https://bitbucket.org/repo/XKyxgx/images/3576625678-click.png)
Attention, selon la configuration de votre ordinateur, il se peut que l'extension (le ".py") ne soit pas être visible
* Un ecran avec les options possible apparait ![s1.png](https://bitbucket.org/repo/XKyxgx/images/4143978869-s1.png)
* Rentrer l'action a executer et suivrent les instructions.

Par example, si on écrit 'liste' et on presse sur enter, un ecran montre la liste des livres existant dans la librairie:![s2.png](https://bitbucket.org/repo/XKyxgx/images/1396033435-s2.png)

Il est aussi possible d'ajouter des nouveaux livres, chercher en utilisant une partie du  titre d'un livre, ou supprimer des livres ajoutés.

### Fichiers de l'application ###
* **librarie.py**: le point d'entrée dans l'application, gére l'interaction avec l'utilisateur et fait appel a librarieDb.py
* **librarieDb.py**: gére l'acces au fichier de base de données
* **librarie.db**: le fichier de base de donées ou tous les informations sont stockées

### Améliorations possibles ###

Celui qui reprend ce code, pourrait ajouter les functinoalités suivantes:
### ###
* Gestion des utilisateurs de la bibliotheque
* Gestion des prêts
* Création d'un interface plus agréabe